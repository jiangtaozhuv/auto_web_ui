from selenium import webdriver

# 创建浏览器对象
driver = webdriver.Firefox()

# 访问新浪
url = 'http://www.baidu.com'
driver.get(url)

# 显示当前的url
print(driver.current_url)

# 显示当前的页面标题
print(driver.title)

# 保存快照操作
# 自动写文件
# driver.get_screenshot_as_file('baidu.jpg')

# 自己写文件
data = driver.get_screenshot_as_png()
with open('baidu2.jpg','wb') as f:
    f.write(data)

driver.close()