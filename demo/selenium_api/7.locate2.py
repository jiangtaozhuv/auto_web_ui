from selenium import webdriver
# 导入By
from selenium.webdriver.common.by import By
import time

driver = webdriver.Firefox()

url = 'http://www.baidu.com'
driver.get(url)

# By调用的定位方式全部为大写
el = driver.find_element(By.ID,'kw')
# 输入搜索关键字(向可输入元素输入文本内容)
el.send_keys('itcast')

# 定位到搜索按钮
el_sub = driver.find_element(By.ID,'su')
# 点击元素
el_sub.click()

time.sleep(3)
driver.close()