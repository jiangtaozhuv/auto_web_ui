# 导包
from selenium import webdriver
import time
# 创建浏览器对象
driver = webdriver.Chrome()

# 访问百度
url1 = 'http://www.baidu.com'
driver.get(url1)
print('访问:',driver.current_url)

# 访问百度
url2 = 'https://zhuanlan.zhihu.com/'
time.sleep(2)
driver.get(url2)
print('访问:',driver.current_url)

# 后退操作
time.sleep(2)
driver.back()
print('后退到',driver.current_url)

# 前进
time.sleep(2)
driver.forward()
print('前进到',driver.current_url)
