from selenium import webdriver

driver = webdriver.PhantomJS()

url = 'http://www.baidu.com'
driver.get(url)

data = driver.page_source
print(type(data))
# 以二进制类型写入文件
with open('baidu.html','wb') as f:
    f.write(data.encode())

# str类型数据(data)转换成bytes类型(二进制类型)
#   b_data = data.encode()
#   data = b_data.decode()

driver.get_screenshot_as_file('baidu3.jpg')