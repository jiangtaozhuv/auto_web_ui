"""
======================
Author: jiangtao.zhu
Time: 2020/8/26 21:01
Project: day1
存放 全局使用的
======================
"""
import os

base_url = "http://192.168.0.179:8235"
# http://120.78.128.25:8765/Index/login.html

# 登陆地址
login_url = os.path.join(base_url,'Index/login.html')
# 主页地址
home_url = os.path.join(base_url,'Index/index.html')

# 通用的普通用户  自动化测试专用用户
user = ("18684720553","python")
