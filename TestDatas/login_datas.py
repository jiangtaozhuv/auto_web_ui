"""
======================
Author: jiangtao.zhu
Time: 2020/8/26 20:53
Project: day1
======================
"""
# 登陆成功的测试数据
valid_user = ("zhujiangtao","lmy123456")


# 登陆失败的数据 - 用户名为空/密码为空/用户格式不正确
invalid_data = [
        {"user":"","passwd":"python","check":"请输入用户名！"},
        {"user": "zhujiangtao", "passwd": "sddfasf", "check": "用户名或密码错误。"},
        {"user": "zhujiangtao", "passwd": "", "check": "请输入密码！"},
    ]


