"""
======================
Author: jiangtao.zhu
Time: 2020/8/24 21:31
Project: day1
脚本编写方式
编写配置项
======================
"""
import pytest
from selenium import webdriver

from TestDatas import global_datas as g_data
from PageObjects.login_page import LoginPage
from Common.my_logger import logger

# 定义fixture - 前置后置 - 作用域 - 返回值
@pytest.fixture(scope="class")
def init():
    # 实例化driver,访问登陆页面
    logger.info("========== class级 前置操作:打开谷歌浏览器,访问系统登陆页面 ===============")
    driver = webdriver.Chrome()
    driver.maximize_window()
    # 打开的站点url存放到全局数据中  该前置条件是 未登录 直接进入登录页面
    driver.get(g_data.login_url)
    yield driver
    logger.info("========== class级 后置操作:关闭谷歌浏览器,退出会话 ===============")
    driver.quit()

@pytest.fixture
def back_login(init):
    # 返回登录页面
    init.get(g_data.login_url)
    yield init

# 前置条件：用户已登陆   后置条件：关闭浏览器
@pytest.fixture(scope="class")
def access(init):
    # 登录后进入首页
    logger.info("========== class级 前置操作:登陆系统 ===============")
    LoginPage(init).login(*g_data.user)
    yield init


# 登录成功后 跳转到首页
@pytest.fixture
def back_home(access):
    access.get(g_data.home_url)
    # 传递的driver句柄
    yield access
